### Punto dentro de un Rectángulo

Se requiere un programa para determinar si un punto está dentro de un Rectángulo:

![Img1](plano1.jpg)


En un sistema GPS es necesario identificar la posición de punto con respecto a un área rectangular. La  posición puede ser:
a) Fuera del rectángulo.
b) Dentro del rectángulo.
c) En el borde superior del rectángulo.
d) En el borde inferior del rectángulo.
e) En el borde izquierdo del rectángulo.
f) En el borde derecho del rectángulo.

Matemáticamente un rectángulo se puede representar mediante la longitud de ancho y alto y las coordenadas (x,y) del punto correspondiente a la esquina superior izquierda.

![Img2](g2.png)

Se requiere un algoritmo cuyas entradas son el ancho, el alto, las coordenadas (cx, cy) de la esquina superior izquierda y las coordenadas (x,y) de un punto. El algoritmo debe determinar si cada una de las siguientes proposiciones es verdadero o falsa:
a) El punto está fuera del rectángulo.
b) El punto está dentro del rectángulo.
c) El punto está en el borde superior del rectángulo.
d) El punto está en el borde inferior del rectángulo.
e) El punto está en el borde izquierdo del rectángulo.
f) El punto está en el borde derecho del rectángulo.


Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
