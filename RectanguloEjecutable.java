
/**
 * Main / ejecutable para un sencillo ejercicio geométrico con un único objeto Rectangulo.
 * @author (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class RectanguloEjecutable
{

    private RectanguloEjecutable(){}
  
    /**
     * Metodo main
     */
    public static void main(String args[]) {
       Rectangulo r = new Rectangulo();
       FrmRectangulo v = new FrmRectangulo(r);
    }

}//fin class TelevisorEjecutable
